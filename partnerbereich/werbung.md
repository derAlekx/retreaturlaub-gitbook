# Werbung

Willkommen auf der Seite "Werbung" von Retreat Urlaub. Hier erfahren Sie, wie Sie Werbeplätze buchen und von den Vorteilen profitieren können, die dies für Ihr Retreat-Angebot bietet.

## Warum Werbung buchen?

Das Schalten von Werbung auf Retreat Urlaub kann Ihre Sichtbarkeit steigern und Ihre Zielgruppe direkt erreichen. Hier sind einige Gründe, warum Sie Werbeplätze auf unserer Plattform buchen sollten:

- **Höhere Sichtbarkeit**: Ihre Retreat-Angebote werden mehr Nutzern angezeigt, was Ihre Chancen auf Buchungen erhöht.

- **Zielgruppengenauigkeit**: Sie können gezielt Nutzer ansprechen, die an Retreats interessiert sind.

- **Anpassungsfähigkeit**: Sie können Ihre Werbeanzeigen flexibel gestalten und je nach Bedarf anpassen.

- **Effizienz**: Das Buchen von Werbeplätzen auf Retreat Urlaub ist einfach und effizient.

## Übersicht über alle geschalteten Werbeplätze

- Auf dieser Seite sehen Sie eine Tabelle mit allen Ihren geschalteten Werbeplätzen.

- Es gibt drei verschiedene Arten von Werbung:
  - **Anzeige auf der Startseite**: Ihre Anzeige wird auf der Startseite der Plattform geschaltet.
  - **Anzeige auf einer Kategorieseite**: Ihre Anzeige wird auf einer bestimmten Kategorieseite platziert.
  - **Anzeige im Newsletter**: Ihre Anzeige wird im Newsletter verschickt.

- **Werbeplatz buchen**: Klicken Sie auf diesen Button, um neue Werbeplätze zu buchen. Dies führt Sie zu einem Wizard, der es Ihnen ermöglicht, die gewünschten Werbeplätze zu erstellen.

- **Editieren (für Newsletter-Anzeigen)**: Wenn Sie eine Anzeige für den Newsletter haben und die geschaltete Woche nachträglich anpassen möchten (solange der Newsletter noch nicht verschickt wurde), klicken Sie auf den "Editieren"-Button.

- **Editieren (für Startseiten-Anzeigen)**: Wenn Sie eine Anzeige für die Startseite haben und das geschaltete Inserat nachträglich ändern möchten, klicken Sie auf den "Editieren"-Button. Dadurch können Sie ein anderes Inserat Ihres Angebots für die Startseite auswählen.

Die Seite "Werbung" bietet Ihnen eine zentrale Stelle zur Verwaltung Ihrer geschalteten Werbeplätze auf der Plattform Retreat Urlaub. Sie können Werbeplätze buchen, bearbeiten und den Inhalt Ihrer Anzeigen anpassen.
