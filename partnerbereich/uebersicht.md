# Übersicht / Dashboard

Die Übersichtsseite des Retreat Urlaub Dashboards bietet Ihnen eine schnelle Zusammenfassung Ihrer Aktivitäten auf der Plattform. Hier sehen Sie wichtige Informationen zu verschiedenen Bereichen.

## Inserate
- **Anzahl der Inserate**: Zeigt die Gesamtanzahl Ihrer Retreat-Angebote.

## Buchungsanfragen
- **Neue Buchungsanfragen**: Zeigt, wie viele neue Anfragen Sie erhalten haben.
- **Offene Buchungsanfragen**: Zeigt die Anzahl der Buchungsanfragen, die noch bearbeitet werden müssen.
- **Offene Rechnungen**: Zeigt ausstehende Rechnungen, die noch beglichen werden müssen.

## Nachrichten
- **Ungelesene Nachrichten**: Zeigt wichtige Nachrichten, die Sie noch nicht gelesen haben.

## Überfällige Anfragen
- **Überfällige Buchungsanfragen**: Zeigt Buchungsanfragen, bei denen der Status noch nicht aktualisiert wurde.

Diese Seite bietet eine klare Übersicht über Ihre Retreat-Aktivitäten und hilft Ihnen, den Überblick zu behalten.

