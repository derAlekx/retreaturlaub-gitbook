# Inserate / Angebote

Die Seite "Inserate" ermöglicht es Ihnen, Ihre Retreat-Angebote zu verwalten. Hier haben Sie eine Übersicht über alle Inserate und verschiedene Funktionen, um diese zu bearbeiten.

## Übersicht über alle Inserate

- **Alle Inserate anzeigen**: Auf dieser Seite sehen Sie eine Tabelle mit all Ihren Retreat-Angeboten.

- **Je Anfragen-Zeile 3 Buttons**:
  - **Ansehen**: Dieser Button führt Sie zur Detailansicht des Angebots, unabhängig vom Status des Angebots.
  - **Editieren**: Mit diesem Button gelangen Sie zur Bearbeitungsansicht des Angebots, um alle Informationen zu ändern.
  - **Löschen**: Durch Klicken auf diesen Button können Sie das jeweilige Angebot löschen.

- **Filterung**:
  - Sie können Ihre Inserate nach verschiedenen Statuskategorien filtern:
    - **Alle**: Zeigt alle Retreat-Angebote an.
    - **Entwurf**: Zeigt die Angebote im Entwurfsstatus.
    - **Veröffentlicht**: Zeigt die veröffentlichten Angebote.

- **Inserat erstellen**: Klicken Sie auf diesen Button, um ein neues Angebot zu erstellen. Sie gelangen zu einem Formular, in dem Sie alle Informationen für das neue Angebot ausfüllen können.

Die Seite "Inserate" bietet Ihnen eine zentrale Stelle zur Verwaltung Ihrer Retreat-Angebote. Sie können vorhandene Angebote bearbeiten, neue Angebote erstellen und den Status Ihrer Angebote filtern.

