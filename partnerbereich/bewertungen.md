# Bewertungen

Die Seite "Bewertungen" bietet Ihnen eine Übersicht über alle Retreat-Bewertungen, die von Teilnehmern abgegeben wurden.

## Übersicht über alle Bewertungen

- **Alle Bewertungen anzeigen**: Auf dieser Seite sehen Sie eine Tabelle mit allen Retreat-Bewertungen.

- **Je Bewertung 1 Button**:
  - **Ansehen**: Durch Klicken auf diesen Button gelangen Sie zur Detailseite der jeweiligen Bewertung.

- **Ansehen**: Klicken Sie auf den "Ansehen"-Button, um zur Detailansicht der jeweiligen Bewertung zu gelangen. Dort finden Sie den gesamten Text der Bewertung.

Die Seite "Bewertungen" ermöglicht es Ihnen, alle von Teilnehmern abgegebenen Bewertungen einzusehen und den vollen Text jeder Bewertung anzuzeigen.
