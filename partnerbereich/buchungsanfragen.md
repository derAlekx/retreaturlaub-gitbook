# Buchungsanfragen

Die Seite "Buchungsanfragen" bietet Ihnen eine Übersicht über alle Retreat-Buchungsanfragen und Funktionen zur Verwaltung dieser Anfragen.

## Übersicht über alle Anfragen

- **Alle Buchungsanfragen anzeigen**: Auf dieser Seite sehen Sie eine Tabelle mit allen Retreat-Buchungsanfragen.

- **Je Anfrage 1 Button**:
  - **Editieren**: Mit diesem Button gelangen Sie zur Bearbeitungsansicht für die jeweilige Buchungsanfrage.

- **Filterung**:
  - Sie können Ihre Buchungsanfragen nach verschiedenen Statuskategorien filtern:
    - **Alle**: Zeigt alle Buchungsanfragen an.
    - **Neue Anfragen**: Zeigt nur neue Buchungsanfragen an.
    - **Offen**: Zeigt Buchungsanfragen im offenen Status an.
    - **Nicht gebucht**: Zeigt Buchungsanfragen an, die noch nicht gebucht wurden.
    - **Gebucht**: Zeigt bereits gebuchte Anfragen an.
    - **Storniert**: Zeigt stornierte Buchungsanfragen an.
    - **Überfällig**: Zeigt Buchungsanfragen, bei denen der Status noch nicht aktualisiert wurde.

- **Suchfeld**:
  - Sie können nach Buchungsanfragen suchen, indem Sie den Nachnamen des Interessenten oder die Buchungsnummer (Anfragenummer) eingeben.

- **Editieren**: Klicken Sie auf diesen Button, um die Bearbeitungsansicht für die jeweilige Buchungsanfrage zu öffnen. Dort können Sie das Direktnachrichtensystem nutzen, um Nachrichten an den Interessenten zu senden.

Die Seite "Buchungsanfragen" ermöglicht es Ihnen, Ihre Retreat-Buchungsanfragen effizient zu verwalten und zu bearbeiten. Sie können Buchungsanfragen nach verschiedenen Kriterien filtern und Nachrichten an Interessenten senden.
