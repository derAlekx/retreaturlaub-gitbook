---
cover: .gitbook/assets/Logo.png
coverY: 0
layout:
  cover:
    visible: true
    size: full
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# 👋 Einleitung

## 👋 Willkommen bei Retreat Urlaub

Willkommen zum Hilfebereich von Retreat Urlaub! Diese Anleitung hilft Ihnen dabei, die Plattform Retreat Urlaub optimal zu nutzen. Retreat Urlaub ermöglicht es Ihnen, Ihr Retreat-Angebot zu präsentieren, Buchungsanfragen zu verwalten, Bewertungen einzusehen und Werbung zu buchen.

{% hint style="success" %}
**Tip:** Sie können auch jederzeit nach einem bestimmten Wort oder einem Problem suchen. Alle Hilfeseiten werden durchsucht um kontexutell passend die richtige Antwort für Sie zu finden.
{% endhint %}

**Inhaltsverzeichnis:**

1. [Übersicht](./#allgemeine-infos-zur-seite)
2. [Die Seiten](./#übersicht-dashboard)
3. [Tipps für die Angebotserstellung](./#inserate-angebote)
4. Troubleshooting
